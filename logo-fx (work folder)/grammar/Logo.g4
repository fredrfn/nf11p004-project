grammar Logo; 

@header {
  package logoparsing;
}

INT : '0' | [1-9][0-9]* ;
WS : [ \t\r\n]+ -> skip ;
IDENTIFICATEUR : ':'[a-zA-Z][a-zA-Z0-9_]*;
BOOLEAN : 'true'|'false';
COMPARISON : '<'|'>'|'=='|'<='|'>=';

exp :
    exp('+'|'-')exp # sum
  |	exp '*' exp # mult
  |	'hasard' exp # hasard
  | atom #atom_rule
;
boolean_exp :
   exp COMPARISON exp # evalbool
;  
atom :
    INT #int
  | '('exp')' # parent
  | 'loop' # loop
  | IDENTIFICATEUR # variable
;

programme : liste_instructions 
;
liste_instructions :
  (instruction)+ 
;
 
instruction :
    IDENTIFICATEUR '=' exp # affect
  | 'si' boolean_exp '['liste_instructions']' '['liste_instructions']' # ternary
  | 'tantque' boolean_exp '['liste_instructions']' # while
  | 'av' exp # av
  | 'td' exp # td
  | 'tg' exp # tg
  | 'lc' # lc
  | 'bc' # bc
  | 've' # ve
  | 're' exp # re
  | 'fpos' '['INT INT']' # fpos
  | 'fcc' exp # fcc
  |	'repete' exp '['liste_instructions']' # repete
;