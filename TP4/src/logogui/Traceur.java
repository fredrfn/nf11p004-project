/*
 * Created on 12 sept. 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package logogui;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class Traceur {
	private static Traceur instance;
	private Group g;
	private double initx = 300, inity = 300;   // position initiale
	private double posx = initx, posy = inity; // position courante
	private int angle = 90;
	private double teta;
	private double etatCrayon = 1;
	private int currentColor = 0; //black
	
	public Traceur() {
		setTeta();
	}

	public void setGraphics(Group g) {
		this.g = g;	
	}
	
	public void clearView() {
		this.g.getChildren().clear();
	}
	
	private int toInt(double a) {
		return (int) Math.round(a);
	}
	
	public void avance(double r) {
		double a = posx + r * Math.cos(teta) ;
		double b = posy - r * Math.sin(teta) ;
		int x1 = toInt(posx);
		int y1 = toInt(posy);
		int x2 = toInt(a);
		int y2 = toInt(b);
		if (etatCrayon == 1) {
		  Line l = new Line(x1,y1,x2,y2);
		  l.setStroke(Color.web(matchHexColor()));
		  g.getChildren().add(l);
		}
		posx = a;
		posy = b;
	}
	
	public void recule(double r) {
		double a = posx - r * Math.cos(teta) ;
		double b = posy + r * Math.sin(teta) ;
		int x1 = toInt(posx);
		int y1 = toInt(posy);
		int x2 = toInt(a);
		int y2 = toInt(b);
		if (etatCrayon == 1) {
			  Line l = new Line(x1,y1,x2,y2);
			  l.setStroke(Color.web(matchHexColor()));
			  g.getChildren().add(l);
			}
		posx = a;
		posy = b;
	}
	
	public void td(double r) {
		angle = (angle - toInt(r)) % 360;
		setTeta();
	}
	
	public void tg(double r) {
		angle = (angle + toInt(r)) % 360;
		setTeta();
	}
	
	public void lc() {
		int etat = 0;
		setEtatCrayon(etat);
	}
	
	public void bc() {
		int etat = 1;
		setEtatCrayon(etat);
	}
	
	public void fpos(double x, double y) {
		posx = x;
		posy = y;
	}
	
	public void fcc(int color) {
		currentColor = color;
	}
	
	public String matchHexColor() {
		String result = "000000";
		switch(currentColor) {
		  case 0:
			  result = "000000";
			  break;
		  case 1:
			  result = "ff0000";
			  break;
		  case 2:
			  result = "00ff00";
			  break;
		  case 3:
			  result = "ffff00";
			  break;
		  case 4:
			  result = "3366cc";
			  break;
		  case 5:
			  result = "ff00ff";
			  break;
		  case 6:
			  result = "0000ff";
			  break;
		  case 7:
			  result = "ffffff";
			  break;
		}
		return result;
	}
	
	private void setTeta() {
		teta = Math.toRadians(angle);
	}
	
	private void setEtatCrayon(double etat) {
		etatCrayon = etat;
	}
}
