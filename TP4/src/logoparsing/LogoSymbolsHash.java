package logoparsing;

import java.util.HashMap;
import java.util.Map;

public class LogoSymbolsHash {
	Map<String, Integer> globals = new HashMap<String, Integer>();
	Map<String, Integer> locals = new HashMap<String, Integer>();

	public Integer addGlobal(String symbol, Integer value) {
		globals.put(symbol, value);
		return 0;
	}
	
	public Integer addLocal(String symbol, Integer value) {
		locals.put(symbol, value);
		return 0;
	}
	
	public Integer getGlobal(String symbol) {
		return globals.get(symbol);
	}
	
	public Integer getLocal(String symbol) {
		return locals.get(symbol);
	}
	
	public Integer cleanLocal(String symbol, Integer value) {
		locals.clear();	
		return 0;
	}	
}
