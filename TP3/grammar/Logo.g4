grammar Logo; 

@header {
  package logoparsing;
}

INT : '0' | [1-9][0-9]* ;
WS : [ \t\r\n]+ -> skip ;

exp :
    exp('+'|'-')exp # sum
  |	exp '*' exp # mult
  |	'hasard' exp # hasard
  | atom #atom_rule
;  
atom :
    INT #int
  | '('exp')' # parent
  | 'loop' # loop
;

programme : liste_instructions 
;
liste_instructions :
  (instruction)+ 
;
 
instruction :
    'av' exp # av
  | 'td' exp # td
  | 'tg' exp # tg
  | 'lc' # lc
  | 'bc' # bc
  | 've' # ve
  | 're' exp # re
  | 'fpos' '['INT INT']' # fpos
  | 'fcc' exp # fcc
  |	'repete' exp '['liste_instructions']' # repete
;