package logoparsing;

import java.util.Random;

import javafx.scene.Group;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import logogui.Log;
import logogui.Traceur;
import logoparsing.LogoParser.Atom_ruleContext;
import logoparsing.LogoParser.AvContext;
import logoparsing.LogoParser.BcContext;
import logoparsing.LogoParser.FccContext;
import logoparsing.LogoParser.FposContext;
import logoparsing.LogoParser.HasardContext;
import logoparsing.LogoParser.IntContext;
import logoparsing.LogoParser.LcContext;
import logoparsing.LogoParser.Liste_instructionsContext;
import logoparsing.LogoParser.LoopContext;
import logoparsing.LogoParser.MultContext;
import logoparsing.LogoParser.ParentContext;
import logoparsing.LogoParser.ReContext;
import logoparsing.LogoParser.RepeteContext;
import logoparsing.LogoParser.SumContext;
import logoparsing.LogoParser.TdContext;
import logoparsing.LogoParser.TgContext;
import logoparsing.LogoParser.VeContext;

public class LogoTreeVisitor extends LogoBaseVisitor<Integer> {
	Traceur traceur;
	ParseTreeProperty<Integer> atts = new ParseTreeProperty<Integer>();
	Integer loop = 1;

	public LogoTreeVisitor() {
		super();
	}
	public void initialize(Group g) {
	      traceur = new Traceur();
	      traceur.setGraphics(g);
    }
	public void setAttValue(ParseTree node, int value) { 
		atts.put(node, value);
	}
	public int getAttValue(ParseTree node) { return atts.get(node); }
	@Override
	public Integer visitAv(AvContext ctx) {
		Integer result = visitChildren(ctx);
		setAttValue(ctx.exp(), result);
		traceur.avance(getAttValue(ctx.exp()));
		Log.appendnl("visitAv, valeur:" + result);
		return 0;
	}
	@Override
	public Integer visitTd(TdContext ctx) { 
		Integer result = visitChildren(ctx);
		setAttValue(ctx.exp(), result);
		traceur.td(getAttValue(ctx.exp()));
		Log.append("visitTd\n" );
		return 0;
	}
	@Override
	public Integer visitTg(TgContext ctx) {
		Integer result = visitChildren(ctx);
		setAttValue(ctx.exp(), result);
		traceur.tg(getAttValue(ctx.exp()));
		Log.append("visitTg\n" );
		return 0;
	}
	
	public Integer visitInt(IntContext ctx) {
		Integer result = Integer.valueOf(ctx.INT().getText());
		setAttValue(ctx, result);
		return result;
	}


	public Integer visitAtom_rule(Atom_ruleContext ctx) {
		Integer result = visitChildren(ctx);
		setAttValue(ctx, result);
		return result;
	}
	
	public Integer visitSum(SumContext ctx) {
		int l = 0;
		int nb = ctx.exp().size();
		System.out.println("nb :" + nb);
		System.out.println("VisitChildren de l'expression:" + ctx.exp(l).getText());
 		Integer result = visit(ctx.exp(l)); //Tant qu'il y a une expression parmi les fils
 		if(result==null) result = Integer.valueOf(getAttValue(ctx));
		l++; int oprow = 1;
		while (l < nb) {
			System.out.println("Dans le while: ajout de l'expression de droite suivante" + ctx.exp(l).getText());
			Integer right = visitChildren(ctx.exp(l));
			System.out.println("l :" + l);
			System.out.println("Result :" + result);
			System.out.println("Right :" + right);
			result = ctx.getChild(oprow).getText().equals("+") ?
			result + right : result - right;
			l++; oprow +=2 ;
		}
		System.out.println("Result :" + result);
		setAttValue(ctx, result);
		return result;
	}

	
	@Override
	public Integer visitMult(MultContext ctx) {
		int l = 0;
		int nb = ctx.exp().size();
		System.out.println("nb :" + nb);
		System.out.println("VisitChildren de l'expression:" + ctx.exp(l).getText());
 		Integer result = visit(ctx.exp(l)); //Tant qu'il y a une expression parmi les fils
 		if(result==null) result = Integer.valueOf(getAttValue(ctx));
		l++; int oprow = 1;
		while (l < nb) {
			System.out.println("Dans le while: ajout de l'expression de droite suivante" + ctx.exp(l).getText());
			Integer right = visitChildren(ctx.exp(l));
			System.out.println("l :" + l);
			System.out.println("Result :" + result);
			System.out.println("Right :" + right);
			result = result * right ;
			l++; oprow +=2 ;
		}
		System.out.println("Result :" + result);
		setAttValue(ctx, result);
		return result;
	}
	@Override
	public Integer visitHasard(HasardContext ctx) {
		System.out.println("Dans hasard : expression " + ctx.exp().getText());
		Integer param = visit(ctx.exp());
	    Random rand = new Random();
	    Integer result = rand.nextInt(param + 1);
		System.out.println("Nombre aleat: " + result);
		setAttValue(ctx, result);
	    return result;
	}
	@Override
	public Integer visitRepete(RepeteContext ctx) {
		Integer i = 0;
		Integer numberOfTimes = visit(ctx.exp());
		loop = numberOfTimes;
		for(i=0;i<numberOfTimes;i++) {
		  visit(ctx.liste_instructions());
		  loop --;
		}
		return 0;
	}
	
	
	public Integer visitListe_instructions(Liste_instructionsContext ctx) {
		// TODO Auto-generated method stub
		return super.visitListe_instructions(ctx);
	}
	public Integer visitParent(ParentContext ctx) {
		System.out.println("Dans parent : expression " + ctx.exp().getText());
		Integer result = visit(ctx.exp());
		System.out.println("Visit Parent result: " + result);
		setAttValue(ctx, result);
		return result;
	}
	public Integer visitLc(LcContext ctx) {
		visitChildren(ctx);
		traceur.lc();
		Log.append("visitLc\n" );
		return 0;
	}
	@Override
	public Integer visitBc(BcContext ctx) {
		visitChildren(ctx);
		traceur.bc();
		Log.append("visitBc\n" );
		return 0;
	}
	@Override
	public Integer visitVe(VeContext ctx) {
		visitChildren(ctx);
		traceur.clearView();
		Log.append("visitVe\n" );
		return 0;
	}
	@Override
	public Integer visitRe(ReContext ctx) {
		Integer result = visitChildren(ctx);
		setAttValue(ctx.exp(), result);
		traceur.recule(getAttValue(ctx.exp()));
		Log.appendnl("visitAv, valeur:" + result);
		return 0;
	}
	@Override
	public Integer visitFpos(FposContext ctx) {
		visitChildren(ctx);
		String x = ctx.INT(0).getText();
		String y = ctx.INT(1).getText();
		Log.appendnl("Reset de la position avec x="+x+"y="+y);
		setAttValue(ctx.INT(0), Integer.valueOf(x));
		setAttValue(ctx.INT(1), Integer.valueOf(x));
		traceur.fpos(Float.valueOf(x),Float.valueOf(y));
		Log.appendnl("visitFpos");
		return 0;
	}
	@Override
	public Integer visitFcc(FccContext ctx) {
		visitChildren(ctx);
		Integer result = visitChildren(ctx);
		setAttValue(ctx.exp(), result);
		traceur.fcc(result);
		Log.appendnl("visitFcc, couleur hex:" + traceur.matchHexColor());
		return 0;
	}
	@Override
	public Integer visitLoop(LoopContext ctx) {
		return loop;
	}

}
